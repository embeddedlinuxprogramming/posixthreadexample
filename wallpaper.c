//
// Created by Juan Bernardo Gómez Mendoza on 11/3/23.
//

#include <unistd.h>
#include <dirent.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "myThreads.h"

void* wallpaperWorker(void *) {
    char command[256];
    DIR *dp;
    struct dirent *ep;
    while(1) {
        dp = opendir ("/root/Pictures");
        if (dp != NULL) {
            while ((ep = readdir (dp)) != NULL) {
                // Check if it is a jpg file.
                int fnLen = strlen(ep->d_name);
                if (fnLen > 5) {
                    char *strPtr = &(ep->d_name[fnLen - 4]);
                    char ext[4] = {strPtr[0], strPtr[1], strPtr[2], strPtr[3]};
                    if (strcmp(strPtr, ".jpg") == 0) {
                        printf("[i] Now showing %s file.\n\r", ep->d_name);
                        sprintf(command, "ffmpeg -i /root/Pictures/%s -pix_fmt bgra -f fbdev /dev/fb0"
                                         " -v 0", ep->d_name);
                        system(command);
                        usleep(20000000);
                    }
                }
            }
            closedir (dp);
        }
        else {
            perror ("Couldn't open the directory");
        }
    }
}