//
// Created by Juan Bernardo Gómez Mendoza on 11/3/23.
//

#ifndef POSIXTHREADEXAMPLE_MYTHREADS_H
#define POSIXTHREADEXAMPLE_MYTHREADS_H

#define MAX_ITER 512  // Can be changed, but works best if a power of 2 greater than or
// equal to 8 is used.

void* ledToggle(void* parm);
void* wallpaperWorker(void*);

#endif //POSIXTHREADEXAMPLE_MYTHREADS_H
