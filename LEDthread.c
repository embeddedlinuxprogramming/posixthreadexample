//
// Created by Juan Bernardo Gómez Mendoza on 11/3/23.
//

#include <stdio.h>
#include <unistd.h>
#include "myThreads.h"

char LED_files[3][39] = { "/sys/class/leds/sunxi_led0r/brightness",
                          "/sys/class/leds/sunxi_led0g/brightness",
                          "/sys/class/leds/sunxi_led0b/brightness"};

int delay[] = { 500, 1000, 2000 };   // Delays in millisecs.

// This function contains the work each thread must do.
void* ledToggle(void* parm) {
    int* ledPtr = parm; // Input parameters should be passed as void*,
    int led = (int)(*ledPtr);   // and then converted into the appropriate type.
    // Variables for the function logic.
    FILE *ledFile = NULL;
    int toggle = 1;
    int it=0;
    // The following line is included for debugging purposes. Can be deleted if desired.
    printf("[I] A thread started working on %s.\n\r"
           "  -> Delay between toggles: %d millisecs.\n\r", LED_files[led],
           delay[led]);
    while(it < (MAX_ITER >> led)) {
        ledFile = fopen(LED_files[led], "w");  // Opens the device in write mode.
        if (ledFile != NULL) {
            fprintf(ledFile, "%d", 5 * toggle);
            fclose(ledFile);    // The device is closed as soon as possible.
            toggle = toggle == 0 ? 1 : 0;  // If toggle == 0 ? it becomes 1; else becomes 0.
            usleep(delay[led] * 1000);
            it++;
        }
        else {
            fprintf(stderr, "[E] Could not open %s. Aborting thread.\n\r", LED_files[led]);
            break;
        }
    }
}