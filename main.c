//
// Created by Juan Bernardo Gómez Mendoza on 11/2/23.
// This demo code is meant to be executed as root on a lichee RV dock.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include "myThreads.h"

int main(int argc, char *const *argv)
{
    // LED threads.
    size_t LED[3];
    pthread_t ledThread[3];
    for (int i=0; i<3; ++i) {
        LED[i] = i;
        if (pthread_create(ledThread + i, NULL, ledToggle,
                           LED + i)) {
            perror("ERROR creating thread.");
            exit(1);
        }
    }
    // Wallpaper thread.
    pthread_t wallpaperThread;
    if (pthread_create(&wallpaperThread, NULL, wallpaperWorker,
                       NULL)) {
        perror("ERROR creating thread.");
        exit(1);
    }
    /* In this part, the user may include other code that will run in parallel with the threads. */
    for (int i=0; i<3; ++i) {
        pthread_join(ledThread[i], NULL);
    }
    pthread_join(wallpaperThread, NULL);
    return 0;
}
